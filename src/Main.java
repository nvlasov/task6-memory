import javassist.CannotCompileException;

public class Main {
    static javassist.ClassPool cp = javassist.ClassPool.getDefault();
    public static void main(String[] args) throws CannotCompileException {
        for (int i = 0; ; i++) {
            Class c = cp.makeClass("Generated" + i).toClass();
            System.out.println(c);
        }
    }
}
